package com.tcwgq.server1.service;

import com.tcwgq.server2api.Service2;
import com.tcwgq.service1api.Service1;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;

@Service
public class Service1Impl implements Service1 {
    @Reference
    Service2 service2;

    public String service() {
        return "Consumer invoke | " + service2.service();
    }

}