package com.tcwgq.server2.service;

import com.tcwgq.server2api.Service2;
import org.apache.dubbo.config.annotation.Service;

@Service
public class Service2Impl implements Service2 {
    @Override
    public String service() {
        return "Provider invoke";
    }

}