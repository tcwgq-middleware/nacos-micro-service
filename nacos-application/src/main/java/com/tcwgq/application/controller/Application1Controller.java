package com.tcwgq.application.controller;

import com.tcwgq.server2api.Service2;
import com.tcwgq.service1api.Service1;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Application1Controller {
    @Reference
    private Service1 service1;

    @Reference
    private Service2 service2;

    @GetMapping("/service1")
    public String service1() {
        return "test" + service1.service();
    }

    @GetMapping("/service2")
    public String service2() {
        return "test" + service2.service();
    }

}